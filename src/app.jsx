import { useState } from 'react';
import { useSelector } from 'react-redux';
import Todo from './components/Todo';

const App = () => {
  const themes = useSelector((state) => state.theme.theme);

  useState(() => {}, [themes]);

  return (
    <main className={`container ${themes}`}>
      <div className="track-1">
        <Todo />
      </div>
    </main>
  );
};

export default App;

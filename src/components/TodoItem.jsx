/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/button-has-type */
/* eslint-disable react/prop-types */
import { useDispatch } from 'react-redux';
import { Modal, TextField, Button } from '@mui/material';
import { MdModeEdit } from 'react-icons/md';
import { Draggable } from 'react-beautiful-dnd';
import { useState } from 'react';
import Swal from 'sweetalert2';
import { toggleTodo, removeTodo, editTodo } from '../redux/todosSlice';

const TodoItem = ({ id, text, completed, index }) => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [editedText, setEditedText] = useState(text);

  const handleToggle = () => {
    dispatch(toggleTodo(id));
  };

  const handleRemove = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'This action will permanently remove the todo item.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, remove it!',
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(removeTodo(id));
      }
    });
  };

  const handleEdit = () => {
    dispatch(editTodo({ id, text: editedText }));
    setIsEditing(false);
  };

  const handleEditClick = () => {
    setIsEditing(true);
  };

  return (
    <Draggable draggableId={id.toString()} index={index}>
      {(provided) => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          className={`todo-item ${completed ? 'completed' : ''}`}
          style={{
            display: 'flex',
            alignItems: 'center',
            userSelect: 'none',
            ...provided.draggableProps.style,
          }}
        >
          {isEditing ? (
            <Modal
              open={isEditing}
              onClose={() => setIsEditing(false)}
              className="custom-modal"
              aria-labelledby="edit-modal-title"
              aria-describedby="edit-modal-description"
            >
              <div className="modal-content">
                <TextField
                  fullWidth
                  className="inputEdit"
                  variant="outlined"
                  label="Edit Todo"
                  value={editedText}
                  onChange={(e) => setEditedText(e.target.value)}
                />
                <Button className="save" variant="contained" color="primary" onClick={handleEdit}>
                  Save
                </Button>
                <Button className="cancel" variant="contained" color="secondary" onClick={() => setIsEditing(false)}>
                  Cancel
                </Button>
              </div>
            </Modal>
          ) : (
            <>
              {/* <input type="checkbox" id="check" checked={completed} onClick={handleToggle} /> */}
              <div className="checkbox__btn">
                <input type="checkbox" id={`check-${id}`} checked={completed} onChange={handleToggle} />
                <label htmlFor={`check-${id}`} />
              </div>
              <label htmlFor="check" />
              <span className="todo-text">{text}</span>
              <MdModeEdit className="iconEdit" onClick={handleEditClick} />
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="remove-btn"
                width="18"
                height="18"
                onClick={handleRemove}
              >
                <path
                  fill="#494C6B"
                  fillRule="evenodd"
                  d="M16.97 0l.708.707L9.546 8.84l8.132 8.132-.707.707-8.132-8.132-8.132 8.132L0 16.97l8.132-8.132L0 .707.707 0 8.84 8.132 16.971 0z"
                />
              </svg>
            </>
          )}
        </div>
      )}
    </Draggable>
  );
};

export default TodoItem;

import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import Swal from 'sweetalert2';
import TodoItem from './TodoItem';
import { removeCompleted, reorderTodos } from '../redux/todosSlice';

const TodoList = () => {
  const todos = useSelector((state) => state.todos);
  const [currentFilter, setCurrentFilter] = useState('all');
  const dispatch = useDispatch();

  const filteredTodos = todos.filter((todo) => {
    if (currentFilter === 'active') {
      return !todo.completed;
    }
    if (currentFilter === 'completed') {
      return todo.completed;
    }
    return true;
  });

  const handleClearCompleted = () => {
    const completedItems = todos.filter((todo) => todo.completed);

    if (completedItems.length === 0) {
      Swal.fire({
        title: 'No Completed Items',
        text: 'You have no completed items to clear.',
        icon: 'info',
      });
    } else {
      Swal.fire({
        title: 'Clear Completed',
        text: 'Yohoo, you already finished your list',
        icon: 'success',
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(removeCompleted());
        }
      });
    }
  };

  const handleOnDragEnd = (result) => {
    if (!result.destination) {
      return;
    }

    const dragIndex = result.source.index;
    const hoverIndex = result.destination.index;

    dispatch(reorderTodos({ dragIndex, hoverIndex }));
  };

  return (
    <>
      <div className="todo-list-container">
        <DragDropContext onDragEnd={handleOnDragEnd}>
          <Droppable droppableId="todoList">
            {(provided) => (
              <ul {...provided.droppableProps} ref={provided.innerRef}>
                {filteredTodos.map((todo, index) => (
                  <TodoItem key={todo.id} {...todo} index={index} />
                ))}
                {provided.placeholder}
              </ul>
            )}
          </Droppable>
        </DragDropContext>
      </div>
      <div className="todo__footer todo_list2">
        <div className="footer1">
          <div className="item1">
            <p>{todos.length} items left</p>
          </div>
          <div className="middle__items">
            <div className="all" onClick={() => setCurrentFilter('all')}>
              <p
                style={{
                  color: currentFilter === 'all' ? 'hsl(220, 98%, 61%)' : '',
                }}
              >
                All
              </p>
              <div className="tool-tip-all">{todos.length} items</div>
            </div>
            <div className="active" onClick={() => setCurrentFilter('active')}>
              <p
                style={{
                  color: currentFilter === 'active' ? 'hsl(220, 98%, 61%)' : '',
                }}
              >
                Active
              </p>
              <div className="tool-tip-active">{todos.filter((todo) => !todo.completed).length} items</div>
            </div>
            <div className="completed" onClick={() => setCurrentFilter('completed')}>
              <p
                style={{
                  color: currentFilter === 'completed' ? 'hsl(220, 98%, 61%)' : '',
                }}
              >
                Completed
              </p>
              <div className="tool-tip-completed">{todos.filter((todo) => todo.completed).length} items</div>
            </div>
          </div>
          <div className="clear" onClick={handleClearCompleted}>
            <p>Clear Completed</p>
          </div>
        </div>
      </div>
      <div className="footer__mobile">
        <div className="all" onClick={() => setCurrentFilter('all')}>
          <p
            style={{
              color: currentFilter === 'all' ? 'hsl(220, 98%, 61%)' : '',
            }}
          >
            All
          </p>
        </div>
        <div className="active" onClick={() => setCurrentFilter('active')}>
          <p
            style={{
              color: currentFilter === 'active' ? 'hsl(220, 98%, 61%)' : '',
            }}
          >
            Active
          </p>
        </div>
        <div className="completed" onClick={() => setCurrentFilter('completed')}>
          <p
            style={{
              color: currentFilter === 'completed' ? 'hsl(220, 98%, 61%)' : '',
            }}
          >
            Completed
          </p>
        </div>
      </div>
    </>
  );
};

export default TodoList;

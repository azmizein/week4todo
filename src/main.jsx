/* eslint-disable import/order */
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { BrowserRouter } from 'react-router-dom';
import ClientRoutes from '@components/ClientRoutes';

import Language from '@containers/Language';

import { store, persistor } from './redux/store';

// import '@styles/core.scss';
// eslint-disable-next-line prettier/prettier
import "./sass/main.scss";
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <DndProvider backend={HTML5Backend}>
        <Language>
          <BrowserRouter>
            <ClientRoutes />
          </BrowserRouter>
        </Language>
      </DndProvider>
    </PersistGate>
  </Provider>
);

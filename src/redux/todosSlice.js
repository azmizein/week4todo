import { createSlice } from '@reduxjs/toolkit';

const todosSlice = createSlice({
  name: 'todos',
  initialState: [],
  reducers: {
    addTodo: (state, action) => {
      state.push({ id: Date.now(), text: action.payload, completed: false });
    },
    editTodo: (state, action) => {
      const { id, text } = action.payload;
      const itemToEdit = state.find((item) => item.id === id);
      if (itemToEdit) {
        itemToEdit.text = text;
      }
    },
    toggleTodo: (state, action) => {
      const todo = state.find((item) => item.id === action.payload);
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    reorderTodos(state, action) {
      const { dragIndex, hoverIndex } = action.payload;
      const [draggedTodo] = state.splice(dragIndex, 1);
      state.splice(hoverIndex, 0, draggedTodo);
    },
    removeCompleted: (state) => state.filter((todo) => !todo.completed),
    removeTodo: (state, action) => state.filter((todo) => todo.id !== action.payload),
  },
});

export const { addTodo, toggleTodo, removeTodo, editTodo, removeCompleted, reorderTodos } = todosSlice.actions;
export default todosSlice.reducer;

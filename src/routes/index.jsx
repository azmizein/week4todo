import MainLayout from '@layouts/MainLayout';

// import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
// import Todo from '@components/Todo';
import App from '../app';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: App,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
